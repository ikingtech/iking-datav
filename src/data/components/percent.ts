import { ComDataType } from '@/data/system-components'

const COM_CDN = ''

export const percent: ComDataType = {
  type: 'percent',
  name: '进度图',
  icon: 'v-icon-relation',
  data: [
    {
      name: 'VBasicPercent',
      alias: '圆环进度图',
      img: `${COM_CDN}/datav/file/com-picture/circle-percent-160-116.png`,
      thum: `${COM_CDN}/datav/file/com-picture/circle-percent-160-116.png`,
      used: true,
    }, {
      name: 'VDamPercent',
      alias: '动画圆环进度图',
      img: `${COM_CDN}/datav/file/com-picture/tree.png`,
      thum: `${COM_CDN}/datav/file/com-picture/tree.png`,
      used: false,
    },
  ],
}
