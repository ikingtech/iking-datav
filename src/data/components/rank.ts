import { ComDataType } from '@/data/system-components'

const COM_CDN = ''

export const rank: ComDataType = {
  type: 'rank',
  name: '排行',
  icon: 'v-icon-relation',
  data: [
    {
      name: 'VScrollRank',
      alias: '基础排行',
      img: `${COM_CDN}/datav/file/com-picture/scroll-rank-332-144.png`,
      thum: `${COM_CDN}/datav/file/com-picture/scroll-rank-332-144.png`,
      used: true,
    }, {
      name: 'VScrollRankBar',
      alias: '排行条形图',
      img: `${COM_CDN}/datav/file/com-picture/scroll-rank-bar-332-144.png`,
      thum: `${COM_CDN}/datav/file/com-picture/scroll-rank-bar-332-144.png`,
      used: true,
    }, {
      name: 'VTableBar',
      alias: '轮播列表柱状图',
      img: `${COM_CDN}/datav/file/com-picture/table-bar-332-144.png`,
      thum: `${COM_CDN}/datav/file/com-picture/table-bar-370-208.png`,
      used: true,
    }, {
      name: 'VRankBoard',
      alias: '轮播条形图',
      img: `${COM_CDN}/datav/file/com-picture/rank-board-332-144.png`,
      thum: `${COM_CDN}/datav/file/com-picture/rank-board-332-144.png`,
      used: true,
    },
  ],
}
