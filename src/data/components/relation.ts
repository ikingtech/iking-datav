import { ComDataType } from '@/data/system-components'

const COM_CDN = ''

export const relation: ComDataType = {
  type: 'relation',
  name: '关系图',
  icon: 'v-icon-relation',
  data: [
    {
      name: 'VTree',
      alias: '树图',
      img: `${COM_CDN}/datav/file/com-picture/tree.png`,
      thum: `${COM_CDN}/datav/file/com-picture/tree.png`,
      used: false,
    },
  ],
}
