
import { ComDataType } from '@/data/system-components'
const COM_CDN = ''
export const customChart: ComDataType = {
  type: 'custom',
  name: '自定义',
  icon: 'v-icon-chart-line',
  data: [
    {
      name: 'VPowerChart',
      alias: '通用Echarts图表',
      img: `${COM_CDN}/datav/file/com-picture/power-chart.png`,
      thum: `${COM_CDN}/datav/file/com-picture/power-chart.png`,
      used: true,
    },
  ],
}
