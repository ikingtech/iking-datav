import { ComDataType } from '@/data/system-components'
const COM_CDN = ''

export const other: ComDataType = {
  type: 'other',
  name: '其他',
  icon: 'v-icon-other',
  data: [
    {
      name: 'VDatePicker',
      alias: '日期选择器',
      img: `${COM_CDN}/datav/file/com-picture/date-picker.jpg`,
      thum: `${COM_CDN}/datav/file/com-picture/time-selector.jpg`,
      used: true,
    },
  ],
}
