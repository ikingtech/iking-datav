import { netPost, netGet } from '@/utils/request'
import axios from 'axios'

export async function getSysTemplates(data?: any) {
  return netPost('/template/page', data )
}

export async function getSysTemplate(id: number) {
  return axios.get(`/templates/tpl-${id}.json`)
}
