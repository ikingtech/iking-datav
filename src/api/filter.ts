/*
 * @Author       : wfl
 * @LastEditors: root zhangpengpeng@ikingtech.com
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:13:00
 * @LastEditTime: 2024-03-21 09:58:00
 */
import { netPost, netGet } from '@/utils/request'

export function filterPage(params){
  return netPost('/filter/select/page',params)
}
export function getFilters(params) {
  return netPost('/filter/lists', params)
}

export function createFilter(data: any) {
  return netPost('/filter/add', data)
}

export function updateFilter(data: any) {
  return netPost(`/filter/update`, data)
}

export function updateFilterName(data: any) {
  return netPost(``, data)
}

export function deleteFilter(id: number | string) {
  return netGet(`/filter/delete?id=${id}`)
}
