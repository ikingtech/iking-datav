/*
 * @Author       : wfl
 * @LastEditors: root zhangpengpeng@ikingtech.com
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-09-26 09:52:24
 * @LastEditTime: 2024-03-25 14:15:23
 */
import { netPost, netGet } from '@/utils/request'
import { updateComs } from './coms'

export function createScreen(data: any) {
  return netPost('/screen/add', data)
}
export function createScreenByTemplate(data: any) {
  return netPost('/screen/template', data)
}

export function getScreen(id: number | string) {
  return netGet(`/screen/get/info?id=${id}`)
}

export function getScreenPage(param: { page: number; rows: number; groupId?: String; name?: String; order?: string; }) {
  return netPost(`/screen/page`, param)
}

export function getScreenByProject(param) {
  return netGet(`/screen/apps`, param)
}

export function updateScreen(data: any) {
  return netPost('/screen/update', data)
}

export function saveScreen(data: any) {
  const resScreen = updateScreen({
    ...data.screen,
    ...data.config,
  })
  const resCom = updateComs(data.coms)

  Promise.all([resScreen, resCom]).then(() => {
    return {
      code: 1000,
      message: 'success',
      data: null,
    }
  })
}
