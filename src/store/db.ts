/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-07 18:57:26
 * @LastEditTime : 2024-01-15 18:26:20
 */
import { defineStore } from 'pinia'
import { dbApi } from '@/api/datebase'

export const useDBStore = defineStore('db', {
  state: () => ({
    dbList: [],
    dbBaseList: [],
    dbTableList: [],
    dbTableKeyList: [],
  }),
  actions: {
    async loadDbs() {
      try {
        const res = await dbApi.queryDb({ page:1, rows: 15 })
        if (res.success) {
          this.dbList = res?.data ?? []
        } else {
          throw Error(res.msg)
        }
      } catch (error) {
        throw error
      }
    },
    async loadBases(param : any) {
      try {
        const res = await dbApi.queryDBBases(param)
        if (res.success) {
          this.dbBaseList = res?.data?.map(item => ({ value: item, label: item })) ?? []
        } else {
          throw Error(res.msg)
        }
      } catch (error) {
        throw error
      }
    },
    async loadTables(param) {
      try {
        const res = await dbApi.queryDBTables(param)
        if (res.success) {
          this.dbTableList = res?.data?.map(item => ({ value: item.name, label: item.name })) ?? []
        } else {
          throw Error(res.msg)
        }
      } catch (error) {
        throw error
      }
    },
    async loadTableKeys(param) {
      try {
        const res = await dbApi.queryDBTableKeys(param)
        if (res.success) {
          this.dbTableKeyList = res?.data ?? []
        } else {
          throw Error(res.msg)
        }
      } catch (error) {
        throw error
      }
    },
  },
})
