import type { App } from 'vue'
import type { SFCWithInstall } from '@/utils/types'
import { loadAsyncComponent } from '@/utils/async-component'
import MessageCarousel from './src/index.vue'

MessageCarousel.install = (app: App): void => {
  app.component('VMessageCarousel', MessageCarousel)
  app.component('VMessageCarouselProp', loadAsyncComponent(() => import('./src/config.vue')))
}

export default MessageCarousel as SFCWithInstall<typeof MessageCarousel>
