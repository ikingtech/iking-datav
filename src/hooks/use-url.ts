
const { VITE_APP_BASE_API, VITE_UPLOAD_URL } = import.meta.env

export const useEnvUrl = () => {
  const cdnUrl = (window as any).ikBasicsUrl
  const setUrl = (inputUrl: string) => {
    // 检查输入是否为空或非字符串，返回空字符串表示无效输入
    if (!inputUrl || typeof inputUrl !== 'string') {
      return ''
    }

    const url = inputUrl.trim()

    // 检查URL是否以http或者https开头，如果是，则不添加CDN前缀
    if (url.startsWith('http') || url.startsWith('https')) {
      return url
    }

    // 确保cdnUrl已定义，否则返回原始URL，并记录警告（或可选地，抛出错误）
    if (typeof cdnUrl === 'undefined') {
      console.warn('cdnUrl is undefined. Returning the original URL.')
      return url
    }

    // 使用模板字符串构建带CDN前缀的URL，同时处理url是否以'/'开头的情况
    return `${cdnUrl}${url.startsWith('/') ? url : `/${url}`}`
  }
  return {
    baseUrl: VITE_APP_BASE_API,
    uploadUrl: VITE_UPLOAD_URL,
    setUrl,
  }
}
